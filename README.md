# Front End Challenges
- This is built with vuepress.  You can read all about that here: https://vuepress.vuejs.org/. 

## To get it running locally:
```bash
$ yarn
$ vuepress dev docs
```

## To deploy:
```bash
# stop other processes, then
$ vuepress build docs
$ git add .
$ git commit -am 'compile to dist'
$ git push origin master
```

Project is live at: https://hcc-challenges-2018.netlify.com/
