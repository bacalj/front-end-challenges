---
Title: Using Google Fonts
---

# Using Google Fonts


_Google fonts are fun and easy and free._ 

---
1. Go to [Google Fonts](https://fonts.google.com/)
2. Choose a font you like and click the + button

![choose a google font](./images/choosegooglefont.png)

3. Go down to the bottom of the page and click the black bar.  Then click "@import" to see the CSS import code. 

4. Highlight the `@import` statement and copy it

![get font code](./images/getcodegooglefont.png)

5. Use the font in your project!

![use font in project](./images/usegooglefont.png)
