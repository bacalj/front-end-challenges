# Welcome

This is a "course" that you can go through at your own pace.  It consists of a series of challenges.  After you complete a challenge, go on to the next one. 

Challenges are meant to be completed with as much or as little help as you like.  As you work through them you can refer to hints by clicking on the 🤔 clues. 

You can also click on the HTML, CSS, and/or JS tabs in the embedded examples to see exactly how do complete a challenge.  Use as much or as little help is best for you and how you like to learn. 

