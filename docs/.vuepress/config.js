module.exports = {
    title: 'Front End Web Basics',
    description: 'To get started with HTML, CSS, and JavaScript',
    themeConfig: {
        nav: [
            { text: 'Home', link: '/'},
            { text: 'About', link: '/about' }
        ],
        sidebar: [
            {
                title: 'Getting Started',
                collapsable: true,
                children: [
                    'setup/what-is-html',
                ]
            },
            {
                title: 'HTML Basics', 
                collapsable: true,
                children: [
                    '/html-basics/italics',
                    '/html-basics/links',
                    '/html-basics/paragraphs-and-headings',
                    '/html-basics/images'
                ]
            },
            {
                title: 'CSS Basics', 
                collapsable: true,
                children: [
                    '/css-basics/selectors',
                    '/css-basics/classes-and-ids',
                    '/css-basics/nested-elements',
                    '/css-basics/hover'
                ]
            },
            {
                title: 'JavaScript Basics', 
                collapsable: true,
                children: [
                    '/js-basics/alert',
                    '/js-basics/use-console',
                    '/js-basics/variables',
                    '/js-basics/conditionals',
                    '/js-basics/arrays'
                ]
            },
            {
                title: 'Projects',
                collapsable: true,
                children: [
                    '/projects/art-gallery',
                    '/projects/six-word-summer',
                    '/projects/css-sandwich',
                    '/projects/doorgame',
                    '/projects/homework-excuse',
                    '/projects/quiz'
                ]
            },
            {
                title: 'Resources',
                collapsable: true,
                children:[
                    'setup/codepen.md',
                    'netlify-deploy',
                    'links',
                    'google-fonts',
                    'share',
                    'codecademy'
                ]
            }
        ]
    }
}
