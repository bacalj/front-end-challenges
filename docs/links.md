---
Title: Other Learning Resources
---

# Other Learning Resources


_There are a ton of free learning resources out there - far too many to list! Some of these are 100% free, and some are free but also tend to bug you to sign up to pay for something._ 

---

- [JavaScript 30](https://javascript30.com/)
You have to sign up, but then these are excellent tutorials and very fun and creative projects. 

- [freeCodeCamp](https://www.freecodecamp.org/)
  100% free and highly detailed

- [codecademy](https://www.codecademy.com/learn)
Walks you through.  Lots of bugging to sign up for paid plan, but exhaust your interest in the free level first. 

- [Khan Academy](https://www.khanacademy.org/computing/computer-programming)
100% free and an incredible amount of content

- [GA Dash](https://dash.generalassemb.ly/)
Great content with videos and interactives - does sign you up for marketing emails from a paid school. 

- [Mozilla Glitch](https://glitch.com/)
   Explore and build full projects.  100% free. 

- [LightBot](https://lightbot.com/flash.html)
This is not web development per se, but this game teaches you to think programatically

- [Scratch](https://scratch.mit.edu/)
  Learn the essentials of programming and graphics editing.  Scratch is used by millions of people of all ages to learn and explore the creative computing. 