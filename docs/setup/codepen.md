---
title: Sign Up for Codepen
---

# Sign up for Codepen

Codepen is a free platofrm where you can write HTML, CSS, and JavaScript and see your results immediately. 

To sign up, you will need an email address. You can use it without signing up, but you can only save your work if you sign up. 

1. Go to [https://codepen.io/](https://codepen.io/) and click "Sign Up" button in the upper right. 

2. If you already use Facebook or Twitter, you can use those credentials to log in or you can just use your email. 

