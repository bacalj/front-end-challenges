---
title: What is a website?
---

# What is a website?

### Do you "go" to a website?

![cartoon of internet](https://bacalj.gitbooks.io/hcc-html-one/content/assets/go_to_site.png)

### Actually, the web site "goes" to your browser when you ask for it by clicking a link. 

![image of files being downloaded](https://bacalj.gitbooks.io/hcc-html-one/content/assets/down_the_files.png)


### Why do we have different types of files?

Each type of file has a certain job. 

#### HTML (.html)

Provides the overall structure, content and "bones" of the site.  You can have a website that is just HTML, but it is not that common anymore. 

#### CSS (.css)

Provides "style" - colors, fonts, shapes, layout. 

#### JavaScript (.js)

Provides "brains" - JavaScript is a programming language. Provides interaction and motion and sometimes helps send data back and forth to and from the server.  