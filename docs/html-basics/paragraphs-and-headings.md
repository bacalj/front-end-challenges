---
Title: Headings and Paragraphs
---

# Paragraphs and Headings

### Goal
Write some text.  Break it up into Heading 1, Heading 2, and paragraph sections. 

<embedpen
    codepenusername="ChallengeGalaxy"
    penhash="mKemVg"
    height="300">
</embedpen>

<clue title="Headings">

The biggest heading is `<h1>`. The next size down is `<h2>`.

</clue>
