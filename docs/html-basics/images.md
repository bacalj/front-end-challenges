---
title: Images
---

# Images

### Goal:

Make an image from another page appear on your page.  Set the width attribute. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="gVpJdw" 
    height="430">
</embedpen>

<clue title="Image tags don't come in pairs">

But as you can see, you can add more than one attribute to an element.  This image has it's "source" attribute set to an image on wikipedia, and it's width attribute set to 400. 

```html
<img width="400" src="https://upload.wikimedia.org/wikipedia/commons/9/95/Naxi_Musicians_I.jpg">
```
</clue>

<clue title="finding image addresses">

An image is like anything else - it has an address where it lives.  For example, the image above lives here:

`https://upload.wikimedia.org/wikipedia/commons/9/95/Naxi_Musicians_I.jpg`

You can find the address of an image by right clicking it and seeing if you get the option to copy its address.  If not, you can keep clicking through until you get to just the image. 

Like this: [musicians](https://upload.wikimedia.org/wikipedia/commons/9/95/Naxi_Musicians_I.jpg)

Some sites have images that you are free to use on your own pages.  A good place to find images like this is [wikimedia commons](https://commons.wikimedia.org/wiki/Category:Images).

</clue>