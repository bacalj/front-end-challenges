---
title: Italics
---

# Italics

### Goal:

Write some text.  Make some words in _italics_. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="KOpLQy" 
    height="220">
</embedpen>

<clue title="Tags often come in pairs">

See how the first `<em>` "starts" the italics and the second "ends" it?

```html
This is regular stuff. <em>And this is in italics</em>. 
```

</clue>