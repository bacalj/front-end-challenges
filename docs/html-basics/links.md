---
title: Links
---

# Links

### Goal:

Write some text.  Make some words link to another site. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="KOpLyX" 
    height="220">
</embedpen>

<clue title="Tags can have attributes">

The `<a> ... </a>` tag surrounds text that will be a link. 

But this kind of tag also has an _attribute_.  In the case of the `<a>` tag the attribute is called `href` - or "hypertext reference", a.k.a. the place where the text links to. 

```html
Look on <a href="www.google.com">Google</a>!
```

</clue>