---
title: About
---

# About

This project was developed by [Joe Bacal](https://github.com/bacalj) in collaboration with [HolyokeCodes](https://holyokecodes.org/) for the _HotJobs in IT_ Community Education program at the [Picknelly Center](https://www.hcc.edu/courses-and-programs/adult-education/picknelly-adult-and-family-education-center) at Holyoke Community College in Holyoke, MA.

The code examples run on [codepen.io](https://codepen.io/trending).

The site was built with a static site generator called [VuePress](https://vuepress.vuejs.org/)

It's hosted for free on [netlify](https://www.netlify.com/). 
