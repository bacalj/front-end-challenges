---
Title: Codecademy Info
---

# Codecademy Info

You can go to [codecademy.com](https://www.codecademy.com), and sign up. 

## Signing Up

1. Once you create your username and password you will be asked to sign on to the "free trial" of the pro account.  This happens automatically.  Just click the big button or anywhere on the screen and you will be taken to your dashboard. 

2. You can try whatever you like on codecademy, but anything with the "pro" label is something that you'll have to pay for eventually.  So, to save you the headache of finding the "good-and-free" stuff that you can keep working on for a few weeks or months without worrying about paying, here are some links to courses that are free on the platform. 

## Courses you can do for free

- [Introduction to HTML](https://www.codecademy.com/learn/learn-html) This one is pretty dry, but if you like to get foundations set first, this covers the basics of HTML. 

- [Introduction to CSS](https://www.codecademy.com/learn/learn-css).  We kind of raced through a bunch of CSS.  Taking this course will solidify and make sense of a number of the things we did in class. 

- [Introduction to JavaScript](https://www.codecademy.com/learn/introduction-to-javascript)
Some of this will be familiar by now, and some will be new.  It goes at a great, easy-to-follow pace. 

- [Learn PHP](https://www.codecademy.com/learn/learn-php)
PHP has been around forever and will be around forever.  Popular systems like WordPress, Drupal, and Moodle are built with PHP.  If you know some PHP you can write WordPress themes and plugins. 

- [Make a Website](https://www.codecademy.com/learn/make-a-website).  This is a bit like what we did in class - it kind of covers some key ideas in HTML and CSS.  Additionally, it shows you how to use Bootstrap, which is a popular CSS framework that you can use to make your site professional looking and easier to organize. 

- [Learn Alexa](https://www.codecademy.com/learn/learn-alexa). This is a pretty cool one.  It starts out getting you thinking about how interactive voice assistants work, and then shows you how to build your own.  It takes time as you have to go out to the AWS platform and do the work there, but looks really cool.  (I have not done it).

- [Learn How to Code](https://www.codecademy.com/learn/learn-how-to-code). This is a cool one - it is really about the concepts that are the same in every programming language - the basic ideas that will help you in any computer-programming related task. 

- [Learn SQL](https://www.codecademy.com/learn/learn-sql).  Databases are everywhere, and understanding how they work and how to "talk" to them is part of the work accross IT.  Knowing some SQL is helpful in all kinds of jobs and applications, and especially in back-end web development. 