---
Title: Conditionals
---

# Conditionals


### Goal

Finally, to finish our pet calculator, we'll use conditionals to make sure our answers make sense. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="gVwpqq" 
    height="250">
</embedpen>