---
Title: Alert and Log
---

# Alert

### Goal

- When one button is clicked, say hi with an alert. 
- When another button is clicked, log something to the console. To view the console you can use the dev tools or click the console botton on codepen. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="VoKwvV" 
    height="430">
</embedpen>

<clue title="I see the alert, but where is the console?">

In codepen, there is a button that says "console.".  You can also find it inside your browser's developer area, if it is turned on.  In Chrome, for example, you right click on the screen, and choose "inspect element". 

</clue>