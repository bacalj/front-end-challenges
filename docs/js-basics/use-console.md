---
Title: Use the Console
---

# Variables and the Console

You can use the console to play and experiment with JavaScript. 

#### First, in your JavaScript editor, define some variables.  
   
Variables are like "handles" that we can create to "hold on" to a value that a program will use.  
   
1. We use the word `const` to declare a variable that is not going to change. 

```js

const cats = 3;
const dogs = 4;

```

2. Click the "console" button at the bottom of the page.

3. Type `dogs` into the console and see what happens.  You should see: 

```shell
> dogs
4
```