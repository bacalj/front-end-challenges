---
Title: Input and Output on the Page
---

# Input and Output on the Page

### Goal 1

Write a function that makes a calculation with two variables. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="BXLNwv" 
    height="200">
</embedpen>


### Goal 2

Make the same thing, except show the result on the page. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="mNrJXj" 
    height="200">
</embedpen>

### Goal 3

Now we will have inputs instead of just "hardcoded" values.

<embedpen   
    codepenusername="joe_bacal" 
    penhash="QeKbxL" 
    height="200">
</embedpen>

Here's the [slides](https://docs.google.com/presentation/d/1nfpD9wv_MurzdfKvonNJ_Ak3V95mr3HvcHtrXj2GOYw/edit?usp=sharing) we looked at in class. 
