---
Title: Arrays
---

# Arrays

### Goal

Create an array, and find values at different indices. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="KOgdaN" 
    height="200">
</embedpen>