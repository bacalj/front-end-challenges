---
Title: Deploy a Site to Netlify
---

# Deploy a Site to Netlify

## Make an account on Netlify

1. Go to [Netlify](https://www.netlify.com/)
2. Click *Sign Up* in the upper right
3. Choose "Email" sign up (unless, of course, you already have a GitHub or Bitbucket account)
   
![image of netlify account creation](./images/createnetlify.png)

4. Check your email to confirm. Click the verify button
5. Go back to Netlify, you should be logged in or able to log in. 
6. Click through the dialogues until you see this:

![image of netlify dashboard](./images/draganddrophere.png)  

## Download your HTML, CSS, and JS files from codepen

1. Log into your codepen account and visit the pen you'd like to deploy to Netlify
2. Click "export" in the lower right
3. Choose .zip
4. Click download .zip

![image of codepen export zip](./images/codepenzip.png)

5. Find the .zip file on your computer - look in the Downloads folder
6. Open it up
7. Find the `dist` folder.  

![image of src and dist directories](./images/src_and_dist.png)

## Upload the files to Netlify

8. Drag the `dist` folder over to the dropzone on netlify where it says "Want to deploy a new site without connecting to Git? Drag and drop your site folder here"

9. Wait for it to build...and voila!

10. You will probably want to change the url to something that makes more sense. In Netlify, go to "Site Settings" for your site. 
    
11. Click the "Change site name" button

![change site name interface](./images/changesitename.png)

13. Now you can visit your site! [Pet Quiz](https://pet-quiz.netlify.com/)