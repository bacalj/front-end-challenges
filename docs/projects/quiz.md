---
Title: Quiz
---

# Quiz

### Goal

- Make a quiz that asks questions and responds intelligently

<embedpen   
    codepenusername="joe_bacal" 
    penhash="dEgNBV" 
    height="530">
</embedpen>

[Slides](https://docs.google.com/presentation/d/1nfpD9wv_MurzdfKvonNJ_Ak3V95mr3HvcHtrXj2GOYw/edit?usp=sharing)