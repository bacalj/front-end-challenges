---
Title: Art Gallery
---

# Art Gallery

### Goal

Style divs and images to create an art gallery. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="MvwyBW" 
    height="430">
</embedpen>