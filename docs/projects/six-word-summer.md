---
Title: Six Word Summer
---

# Six Word Summer

### Goal

Write a poem about your Summer that is six words long. Use HTML and CSS to create a cool look. Here's another example: [My Six Word Summer](https://thimbleprojects.org/mozillalearning/11703/)

<embedpen   
    codepenusername="joe_bacal" 
    penhash="dxPEbE" 
    height="430">
</embedpen>