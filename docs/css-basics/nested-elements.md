---
Title: Divs and Nested Elements
---

# Divs and Nested Elements

### Goal:

Put different words in different divs.  Make the divs different sizes and colors. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="wVMLjq" 
    height="430">
</embedpen>

<clue title="use nesting to make it easy on your brain">

This html

```html
<div class="happy"><p>Plese read this <em>article</em></p></div>
```

is the same as this html
```html
<div class="happy">
    <p>
        Plese read this <em>article</em>
    </p>
</div>
```

But one is easier to understand.  We can see elements fit inside other elements. 

</clue>

<clue title="styles cascade down">

If you write css for all `p` elements - it will apply to all of them, no matter their class. 

That's why in the example above, the two divs the size and shape we defined in the `div` selector, but they use the colors we assigned to their classes.

</clue>

<clue title="Color codes">

You probably noticed we can use some color words - but there are ways to find and use color codes that have much more variety. 

```css
.warning {
    background-color:#FDEDBB;
}
```
You can find color codes lots of different ways.  One place to start is at [adobe color](https://color.adobe.com/create)

</clue>