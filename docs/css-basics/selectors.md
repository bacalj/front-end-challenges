---
Title: Selectors
---

# Selectors

### Goal:

Make different elements appear with different colors. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="WVrBJw" 
    height="430">
</embedpen>

<clue title="Selectors tell tags how to look">

In our `html` file we use html to say: _Make a paragraph that says "Hello World"_
```html
<p>Hello, World</p>
```
In our `css` file we use css to say: _Hey, calling all paragraphs! Your color is blue!_
```css
p {
    color:blue;
}
```
</clue>



<clue title="Keep your curly braces organized">

This is like saying, "Hey all you paragraphs, follow the rules on my list of rules.  The list always starts with a { and ends with a }
```css
p {
    color:blue;
}
```
</clue>