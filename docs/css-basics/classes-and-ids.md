---
Title: Classes and Ids
---

# Classes and Ids

### Goal:

Make two elements of the same type appear differently. They should use different fonts and different colors. For example, one red paragraph, and one green paragraph. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="PMZvLq" 
    height="430">
</embedpen>

<clue title="dots and number signs">

This css selector "talks" to all elements with the `relaxed` class. 
```css
.relaxed {
    color:purple;
}
```

This css selector "talks" to the one element with the id of `best`.

```css
#best {
    color:blue;
}
```
</clue>

<clue title="You can make up class names">

Classes become important, so it's good to give them names that make for what the function of the text is. 

```html
<p class="announcement">Hello, World</p>
```

```css
p {
    color:red;
}
```
</clue>