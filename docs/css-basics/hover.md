---
Title: Hover Effects
---

# Hover Effect

### Goal

Make an inviting button that changes how it looks when you hover over it. 

<embedpen   
    codepenusername="joe_bacal" 
    penhash="dxGxOv" 
    height="430">
</embedpen>

<clue title="transitions make things go smoothly">

You can see what it looks like without the transition by "commenting out" some lines of code.  If you put `//` before a line of CSS the computer will ignore it.  Try commenting out the transition lines and see the difference.

```css
button {
  padding:30px;
  border-radius:20px;
  border:0px;
  background-color:lightblue;
  //transition-property:all;
  //transition:.3s;
}
```
</clue>